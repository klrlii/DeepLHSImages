from settings import (
    cuda_info,
    save_model,
    save_plots,
    CNN,
    StrongCNN,
    train_validate,
    train_loader,
    valid_loader,
)
from torch import nn, optim
import numpy as np

if __name__ == "__main__":
    supported = cuda_info()
    net = StrongCNN().to("cuda")
    str = net.__str__()
    with open("model_params.txt", "w") as file:
        file.write(str + "\n")
        file.close()

    iterations = 20
    num_of_epochs = 50

    train_loss_mean = np.zeros(num_of_epochs)
    train_acc_mean = np.zeros(num_of_epochs)
    valid_loss_mean = np.zeros(num_of_epochs)
    valid_acc_mean = np.zeros(num_of_epochs)

    train_loss_std = np.zeros(num_of_epochs)
    train_acc_std = np.zeros(num_of_epochs)
    valid_loss_std = np.zeros(num_of_epochs)
    valid_acc_std = np.zeros(num_of_epochs)

    for i in range(iterations):
        print(f"Current iteration: {i + 1}")

        net = None
        device = None

        if supported:
            device = "cuda"
            net = StrongCNN().to(device)
        else:
            device = "cpu"
            net = StrongCNN().to(device)

        loss_function = nn.CrossEntropyLoss(reduction="none", size_average=False)
        learning_rate = 1e-3
        optimizer = optim.Adam(net.parameters(), lr=learning_rate)

        (
            epochs,
            train_losses,
            train_accuracys,
            valid_losses,
            valid_accuracys,
        ) = train_validate(
            model=net,
            loss_function=loss_function,
            optimizer=optimizer,
            num_of_epochs=num_of_epochs,
            trainloader=train_loader,
            testloader=valid_loader,
            device=device,
        )
        train_losses = np.array(train_losses)
        train_accuracys = np.array(train_accuracys)
        valid_losses = np.array(valid_losses)
        valid_accuracys = np.array(valid_accuracys)

        train_loss_mean += train_losses / iterations
        train_acc_mean += train_accuracys / iterations
        valid_loss_mean += valid_losses / iterations
        valid_acc_mean += valid_accuracys / iterations

        train_loss_std = np.vstack((train_loss_std, train_losses))
        train_acc_std = np.vstack((train_acc_std, train_accuracys))
        valid_loss_std = np.vstack((valid_loss_std, valid_losses))
        valid_acc_std = np.vstack((valid_acc_std, valid_accuracys))

    train_loss_std = np.delete(train_loss_std, (0), axis=0)
    train_acc_std = np.delete(train_acc_std, (0), axis=0)
    valid_loss_std = np.delete(valid_loss_std, (0), axis=0)
    valid_acc_std = np.delete(valid_acc_std, (0), axis=0)

    train_loss_err = []
    train_acc_err = []
    valid_loss_err = []
    valid_acc_err = []

    for i in range(num_of_epochs):
        column_train_loss = train_loss_std[:, i]
        column_train_acc = train_acc_std[:, i]
        column_valid_loss = valid_loss_std[:, i]
        column_valid_acc = valid_acc_std[:, i]

        train_loss_err.append(np.std(column_train_loss))
        train_acc_err.append(np.std(column_train_acc))
        valid_loss_err.append(np.std(column_valid_loss))
        valid_acc_err.append(np.std(column_valid_acc))

    train_loss_err = np.array(train_loss_err)
    train_acc_err = np.array(train_acc_err)
    valid_loss_err = np.array(valid_loss_err)
    valid_acc_err = np.array(valid_acc_err)

    save_plots(
        num_of_epochs=num_of_epochs,
        train_loss_mean=train_loss_mean,
        valid_loss_mean=valid_loss_mean,
        train_acc_mean=train_acc_mean,
        valid_acc_mean=valid_acc_mean,
        train_loss_err=train_loss_err,
        valid_loss_err=valid_loss_err,
        train_acc_err=train_acc_err,
        valid_acc_err=valid_acc_err,
    )

    # save_model(num_of_epochs, net, optimizer, loss_function)
