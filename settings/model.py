from torch.nn import Module
from torch import nn, max, no_grad
from torch.nn import functional as F
import numpy as np
from tqdm import trange, tqdm


class CNN(Module):
    def __init__(self) -> None:
        super(CNN, self).__init__()
        self.conv1 = nn.Conv2d(in_channels=3, out_channels=6, kernel_size=5)
        self.conv2 = nn.Conv2d(in_channels=6, out_channels=16, kernel_size=5)
        self.pool = nn.MaxPool2d(kernel_size=2, stride=2)
        self.fc1 = nn.Linear(16 * 69 * 69, 256)
        self.fc2 = nn.Linear(256, 128)
        self.fc3 = nn.Linear(128, 64)
        self.fc4 = nn.Linear(64, 32)
        self.fc5 = nn.Linear(32, 16)
        self.fc6 = nn.Linear(16, 8)
        self.fc7 = nn.Linear(8, 4)
        self.fc8 = nn.Linear(4, 2)

    def forward(self, x: np.ndarray):
        x = self.pool(F.relu(self.conv1(x)))
        x = self.pool(F.relu(self.conv2(x)))
        x = x.view(-1, 16 * 69 * 69)
        x = F.relu(self.fc1(x))
        x = F.relu(self.fc2(x))
        x = F.relu(self.fc3(x))
        x = F.relu(self.fc4(x))
        x = F.relu(self.fc5(x))
        x = F.relu(self.fc6(x))
        x = F.relu(self.fc7(x))
        x = self.fc8(x)
        return x


class StrongCNN(nn.Module):
    def __init__(self):
        super(StrongCNN, self).__init__()

        self.pool = nn.MaxPool2d(kernel_size=2, stride=2)
        self.dropout = nn.Dropout(p=0.2)
        self.conv1 = nn.Conv2d(in_channels=3, out_channels=8, kernel_size=5)
        self.bn1 = nn.BatchNorm2d(8)
        self.conv2 = nn.Conv2d(in_channels=8, out_channels=16, kernel_size=1)
        self.bn2 = nn.BatchNorm2d(16)
        self.conv3 = nn.Conv2d(in_channels=16, out_channels=16, kernel_size=3)
        self.bn3 = nn.BatchNorm2d(16)
        self.conv4 = nn.Conv2d(in_channels=16, out_channels=32, kernel_size=1)
        self.bn4 = nn.BatchNorm2d(32)
        self.conv5 = nn.Conv2d(in_channels=32, out_channels=32, kernel_size=3)
        self.bn5 = nn.BatchNorm2d(32)

        self.fc1 = nn.Linear(32 * 68 * 68, 128)
        self.fc2 = nn.Linear(128, 2)

    def forward(self, x):
        x = self.bn1(F.relu(self.conv1(x)))
        x = self.pool(x)
        x = self.bn2(F.relu(self.conv2(x)))
        x = self.bn3(F.relu(self.conv3(x)))
        x = self.pool(x)
        x = self.bn4(F.relu(self.conv4(x)))
        x = self.bn5(F.relu(self.conv5(x)))
        x = x.view(-1, 32 * 68 * 68)
        x = F.relu(self.fc1(x))
        x = self.dropout(x)
        x = self.fc2(x)
        return x


def train_validate(
    model: Module,
    loss_function,
    optimizer,
    num_of_epochs,
    trainloader,
    testloader,
    device,
):
    epochs = range(num_of_epochs)
    train_losses = []
    train_accuracys = []
    valid_losses = []
    valid_accuracys = []
    for epoch in range(num_of_epochs):
        print(f"Epoch: {epoch}")
        model.train()
        train_running_loss = 0.0
        train_running_acc = 0.0
        counter = 0
        print("Training...")
        for i, batch in tqdm(enumerate(trainloader), total=len(trainloader)):
            X_batch, y_batch = batch
            X_batch = X_batch.to(device)
            y_batch = y_batch.to(device)

            optimizer.zero_grad()
            y_pred = model(X_batch)
            loss = loss_function(y_pred, y_batch)
            train_running_loss += loss.item()
            _, preds = max(y_pred.data, 1)
            train_running_acc += (preds == y_batch).sum().item()

            loss.backward()
            optimizer.step()
            counter += 1
        train_losses.append(train_running_loss / counter)
        train_accuracys.append(train_running_acc / len(trainloader.dataset))

        model.eval()
        valid_running_loss = 0.0
        valid_running_acc = 0.0
        counter = 0
        print("Validating...")
        with no_grad():
            for i, batch in tqdm(enumerate(testloader), total=len(testloader)):
                X_batch, y_batch = batch
                X_batch = X_batch.to(device)
                y_batch = y_batch.to(device)

                y_pred = model(X_batch)
                loss = loss_function(y_pred, y_batch)
                valid_running_loss += loss.item()
                _, preds = max(y_pred.data, 1)
                valid_running_acc += (preds == y_batch).sum().item()
                counter += 1
        valid_losses.append(valid_running_loss / counter)
        valid_accuracys.append(valid_running_acc / len(testloader.dataset))

    return epochs, train_losses, train_accuracys, valid_losses, valid_accuracys
