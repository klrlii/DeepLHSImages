import cv2
import numpy as np
import os
from tqdm import tqdm


def brightness(img: np.ndarray):
    if len(img.shape) == 3:
        # Colored RGB or BGR (*Do Not* use HSV images with this function)
        # create brightness with euclidean norm
        return np.average(np.linalg.norm(img, axis=2)) / np.sqrt(3)
    else:
        # Grayscale
        return np.average(img)


directory = "M:/NIR/try_2_tensorflow/settings"

tif_files = []
for root, dirs, files in os.walk(directory):
    # select file name
    for file in files:
        # check the extension of files
        if file.endswith(".tif"):
            # print whole path of files
            tif_files.append(os.path.join(root, file))

images = []  # [cv2.imread(filename, cv2.IMREAD_GRAYSCALE) for filename in tif_files]
for filename in tqdm(tif_files):
    cv2.imwrite(filename, cv2.resize(cv2.imread(filename), (300, 300)))

i = 0
total_deleted = 0
for image in tqdm(images):
    br = brightness(image)
    if br < 8:
        os.remove(tif_files[i])
        total_deleted += 1
    i += 1

print(total_deleted)
