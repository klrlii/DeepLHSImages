from torchvision.datasets import ImageFolder
from torchvision.transforms import (
    Compose,
    ToTensor,
    Resize,
)
from torch.utils.data import DataLoader


BATCH_SIZE = 32

train_transforms = Compose(
    [
        Resize((290, 290)),
        ToTensor(),
    ]
)
valid_transforms = Compose(
    [
        Resize((290, 290)),
        ToTensor(),
    ]
)

train_dataset = ImageFolder(root="./settings/dataset/train", transform=train_transforms)
valid_dataset = ImageFolder(root="./settings/dataset/valid", transform=valid_transforms)

train_loader = DataLoader(
    train_dataset, batch_size=BATCH_SIZE, shuffle=True, pin_memory=True
)
valid_loader = DataLoader(
    valid_dataset, batch_size=BATCH_SIZE, shuffle=False, pin_memory=True
)
