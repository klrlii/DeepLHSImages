import torch
import matplotlib.pyplot as plt


def cuda_info() -> bool:
    is_support = torch.cuda.is_available()
    if is_support:
        print("CUDA is supported by this system")
    else:
        print(
            "CUDA is not supported by this system\nYou should install: pip3 install torch torchvision torchaudio --index-url https://download.pytorch.org/whl/cu118"
        )
        return False
    print("CUDA version:", torch.version.cuda)
    device_id = torch.cuda.current_device()
    print(f"ID of current CUDA device: {device_id}")
    print(f"Name of current CUDA device: {torch.cuda.get_device_name(device_id)}")
    return True


def save_model(epochs, model, optimizer, criterion):
    torch.save(
        {
            "epoch": epochs,
            "model_state_dict": model.state_dict(),
            "optimizer_state_dict": optimizer.state_dict(),
            "loss": criterion,
        },
        "./model/model.pth",
    )


def save_plots(
    num_of_epochs,
    train_loss_mean,
    valid_loss_mean,
    train_acc_mean,
    valid_acc_mean,
    train_loss_err,
    valid_loss_err,
    train_acc_err,
    valid_acc_err,
):
    plt.style.use("ggplot")
    plt.plot(range(num_of_epochs), train_loss_mean, color="orange")
    plt.plot(range(num_of_epochs), valid_loss_mean, color="red")
    plt.errorbar(
        range(num_of_epochs),
        train_loss_mean,
        fmt="o",
        color="orange",
        ecolor="orange",
        yerr=train_loss_err,
        label="Train loss",
    )
    plt.errorbar(
        range(num_of_epochs),
        valid_loss_mean,
        fmt="o",
        color="red",
        ecolor="red",
        yerr=valid_loss_err,
        label="Validation loss",
    )
    plt.legend()
    plt.savefig("./graphs/loss_30_iter.png")
    plt.close()

    plt.plot(range(num_of_epochs), train_acc_mean, color="orange")
    plt.plot(range(num_of_epochs), valid_acc_mean, color="red")
    plt.errorbar(
        range(num_of_epochs),
        train_acc_mean,
        fmt="o",
        color="orange",
        ecolor="orange",
        yerr=train_acc_err,
        label="Train accuracy",
    )
    plt.errorbar(
        range(num_of_epochs),
        valid_acc_mean,
        fmt="o",
        color="red",
        ecolor="red",
        yerr=valid_acc_err,
        label="Validation accuracy",
    )
    plt.legend()
    plt.savefig("./graphs/accuracy_30_iter.png")
    plt.close()
